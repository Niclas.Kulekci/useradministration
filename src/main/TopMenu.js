import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import SearchIcon from '@material-ui/icons/Search';
import {Link} from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: '10px'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function TopMenu() {
    const classes = useStyles();
    return (
        <AppBar position="static" classes={classes.root}>
            <Toolbar>
                <Button component={Link} to="/">
                    <HomeIcon/>
                </Button>
                <Button component={Link} to="/SearchUsers">
                    Users
                </Button>
                <Button component={Link} to="/SearchRights">
                    Rights
                </Button>
                <Button  component={Link} to="/SearchDomains">
                    Domains
                </Button>
            </Toolbar>
        </AppBar>
    );
}