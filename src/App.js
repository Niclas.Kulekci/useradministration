import './App.css';
import React from 'react';
import DoChanging from "./Users/EDIT";
import DoChanging2 from "./Domains/EDIT_domain";
import HomePage from './pages/HomePage'
import {Domains} from './Domains/Domains'
import {Users} from './Users/Users'
import Rights from './Rights/Rights'
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import TopMenu from "./main/TopMenu";



function App() {
  return (
      <BrowserRouter>
        <TopMenu/>
        <div style={{marginTop: '20px', marginBottom: '20px'}}>

          <Switch>
              <Route path="/SearchDomains">
                  <Domains/>
              </Route>
            <Route path="/SearchUsers">
              <Users/>
            </Route>
            <Route path="/SearchRights">
                <Rights/>
            </Route>
              <Route path="/SearchEdit">
                  <DoChanging/>
              </Route>
              <Route path="/SearchEdit2">
                  <DoChanging2/>
              </Route>
            <Route path="/">
              <HomePage/>
            </Route>
          </Switch>

        </div>

      </BrowserRouter>
  );
}

export default App;