import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";

export class EditDomainDialogue extends Component {
    constructor(props) {
        super(props);
    }

    editDomain(event) {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains/'+ event.target.id.value,
            {
                method: 'POST',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: event.target.id.value,
                    domain: event.target.domain.value
                })
            })

            .then(res=> res.json())
            .then((result) =>{alert('Success!');}
            )
    }

    render() {
        return (
            <Dialog open={this.props.open} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Edit Domain</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <form onSubmit={this.editDomain}>
                        <TextField
                            name='id'
                            disabled
                            type="hidden"
                            defaultValue={this.props.domid}
                        />
                        <br/>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            name='domain'
                            label="domain"
                            type="text"
                            defaultValue={this.props.domain}
                        />
                        <br/>
                        <Button variant="contained" color="primary" type="submit" onClick={this.props.editClose}>
                            update
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.editClose}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

}
