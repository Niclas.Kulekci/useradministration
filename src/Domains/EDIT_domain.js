import React, {Component} from "react";
import {EditDomainDialogue} from "./Dialogue_EDIT_Domain";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import Grid from "@material-ui/core/Grid";
import SearchBox2 from "./Search";


function searchingFor(searchDomain) {
    return function (val) {
        return val.domain.toLowerCase().includes(searchDomain.toLowerCase());
    }
}

class editPage2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: [],
            domainPopDialogue: false,
            searchDomain: '',
        }
    }

    deleteDomain(id) {
        fetch('https://usersjava20.sensera.se/domains/' + id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(result => {
            alert('Success!');
        })
    }

    deleteDomainRights(id,rightName) {
        fetch('https://usersjava20.sensera.se/domains/' + id + '/' + rightName, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id,
                rightName: rightName
            })

        }).then(result => {
            alert('Success!');
        })
    }


    searchHandlerDom = (event, searchDomain) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains/find?domain=' + searchDomain)
            .then(response => response.json())
        this.setState({searchDomains: event.target.value})
    }

    refreshList = () => {
        fetch('https://usersjava20.sensera.se/domains')
            .then(response => response.json())
            .then(data => {
                this.setState({domains: data});
            })

    }

    componentDidMount = () => {
        this.refreshList();
    }

    render() {
        const {domains} = this.state
        let EditDialogClose = () => this.setState({domainPopDialogue: false});

        return (
            <Container>
                <EditDomainDialogue
                    open={this.state.domainPopDialogue}
                    domain={this.state.domain}
                    domid={this.state.id}
                    editClose={EditDialogClose}/>


                <SearchBox2 searchHandlerDom={this.searchHandlerDom}/>

                <Paper component="nav" aria-label="main mailbox folders" style={{
                    marginBottom: '10px',
                    overflow: 'auto',
                    maxHeight: 700,
                }}>
                    <Grid container spacing={1}>
                        <Grid>
                            <List>
                                {domains.filter(searchingFor(this.state.searchDomain)).map((val) =>
                                    <ListItem key={val.id} style={{height: '70px'}}>
                                        <ListItemText primary={val.domain}/>
                                    </ListItem>
                                )}
                            </List>
                        </Grid>

                        <Grid>
                            <List>
                                {domains.filter(searchingFor(this.state.searchDomain)).map((val) =>
                                    <ListItem key={val.id} style={{marginLeft: '100px', height: '70px'}}>

                                        <DialogActions style={{marginTop: '40px'}}>
                                            <Button size="small" variant="contained" color="primary"
                                                    style={{marginTop: '-70px', float: 'right',}}
                                                    onClick={() => this.setState({
                                                        domainPopDialogue: true, id: val.id,
                                                        domain: val.domain
                                                    })}
                                            >
                                                Edit
                                            </Button>
                                        </DialogActions>

                                        <DialogActions style={{marginTop: '40px'}}>
                                            <Button size="small" variant="contained" color="secondary"
                                                    style={{marginTop: '-70px', float: 'right',}}
                                                    onClick={() => this.deleteDomain(val.id)}
                                            >
                                                Delete
                                            </Button>
                                        </DialogActions>

                                        <DialogActions style={{marginTop: '40px'}}>
                                            <Button size="small" variant="contained" color="secondary"
                                                    style={{marginTop: '-70px', float: 'right',}}
                                                    onClick={() => this.deleteDomainRights(val.id,val.rightName)}
                                            >
                                                DeleteRights
                                            </Button>
                                        </DialogActions>
                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                    </Grid>
                </Paper>
            </Container>
        );
    }
}

export default editPage2;

