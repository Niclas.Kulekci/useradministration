import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import {AddDomainDialogue} from "./Dialogue_ADD_Domain";
import {Link} from "react-router-dom";
import SearchBox2 from "./Search";


function searchingFor(searchDomain) {
    return function (val) {
        return val.domain.toLowerCase().includes(searchDomain.toLowerCase());
    }
}

export class Domains extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: [],
            domainPopDialogue: false,
            searchDomain: '',
        }
    }

    searchHandlerDom = (event, searchDomain) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains/find?domain=' + searchDomain)
            .then(response => response.json())
            .then(date => {
                this.setState({domain: date});
            })
        this.setState({searchDomain: event.target.value})
    }

    refreshList = () => {

        fetch('https://usersjava20.sensera.se/domains')
            .then(response => response.json())
            .then(date => {
                this.setState({domains: date});
            })
    }

    componentDidMount = () => {
        this.refreshList();
    }

    render() {
        const {domains} = this.state
        let addDomainDialogueClose = () => this.setState({domainPopDialogue: false});

        return (
            <Container>
                <SearchBox2 searchHandlerDom={this.searchHandlerDom}/>
                <Paper elevation={1} component="form">
                    <List component="nav" aria-label="main mailbox folders" style={{
                        marginBottom: '10px',
                        overflow: 'auto',
                        maxHeight: 700,
                    }}>
                        {domains.filter(searchingFor(this.state.searchDomain)).map((val) =>
                            <ListItem key={val.id} style={{}}>
                                <ListItemText primary={val.domain}/>
                            </ListItem>
                        )}
                    </List>

                </Paper>
                <Button variant="contained" color="primary"
                        onClick={() => this.setState({domainPopDialogue: true})}>
                    Add Domain
                </Button>
                <Button variant="contained" color="secondary" component={Link} to="/SearchEdit2" style={{marginLeft:'30px'}}>
                    Edit
                </Button>

                <AddDomainDialogue
                    open={this.state.domainPopDialogue}
                    onClick={addDomainDialogueClose}
                />
            </Container>

        );
    }
}
