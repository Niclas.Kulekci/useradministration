import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import SearchBox from "./Search";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Divider from "@material-ui/core/Divider";
import CloseIcon from '@material-ui/icons/Close';

function SearchThis (searchUsers) {
    return function (val){
        return val.username.toLowerCase().includes(searchUsers.toLowerCase());
    }
}

export class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            popupDialogue: false,
            popupDialogue2: false,
            searchUser: '',

        }
    }

    componentDidMount=()=> {
        this.refreshList();
    }

    refreshList=()=> {
        fetch('https://usersjava20.sensera.se/users')
            .then(response => response.json())
            .then(data => {
                this.setState({users: data});
            })

    }

    searchHandler = (event, searchUser) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/find?username=' + searchUser)
            .then(response => response.json())
        this.setState({searchUser: event.target.value})
    }

    addUser = (event) => {
        event.preventDefault();

        fetch('https://usersjava20.sensera.se/users',
            {
                method: 'POST',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: event.target.username.value, password: 'UnCrackable'
                })
            })

            .then(res=> res.json())
            .then((res) =>{this.setState({popupDialogue: false})} //setting the dialogue state here frees up onclick for buttons
            )

    }

    editUser = (event) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users',
            {
                method: 'POST',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: event.target.username.value, password:'crackThis'
                })
            })

            .then(res=> res.json())
            .then((res) =>{this.setState({popupDialogue2: false})}
            )

        //Question is if "post" is the correct method to use, why isnt this working?????
        // Does it need an ID to know which entry to override?
        //todo: fix this functions
    }

    deleteUser = (event, id) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/' + id,
            {
                method: 'DELETE',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'
                },

                /*body: JSON.stringify({
                    username: event.target.username.value
                })
                */
            })

            .then(res=> res.json())
            .then((res) =>{alert('Deleted');},
            )
        //Assumptions are that program/function is unclear on what "id" actually is in this context
        //todo:fix this function, dont forget about line 172
    }

    render() {
        let userCatalog = this.state.users.filter(SearchThis(this.state.searchUser)).map((val) => {
            return (

                <List component="nav" aria-label="main mailbox folders" key={val.id}>
                    <ListItem>

                        <ListItemText primary={val.username} secondary={val.id}/>

                        <Dialog open={this.state.popupDialogue2} aria-labelledby="simple-dialog-title">
                            <DialogTitle id="simple-dialog-title">Edit user</DialogTitle>
                            <DialogContent style={{width: '500px'}}>
                                <form onSubmit={this.editUser}>
                                    <TextField
                                        required
                                        id="username"
                                        name='username'
                                        label="edit"
                                        type="text"
                                        defaultValue={this.state.username}
                                    />
                                    <br/>
                                </form>
                            </DialogContent>
                            <DialogActions>
                                <Button variant="contained" color="primary" type="submit"
                                        onClick={() => this.setState({popupDialogue2: false})}>
                                    Edit User
                                </Button>

                                <Button variant="contained" color="secondary"
                                        onClick={() => this.setState({popupDialogue2: false})}>
                                    Cancel
                                </Button>
                            </DialogActions>
                        </Dialog>

                        <DialogActions style={{marginTop: '40px'}}>
                            <Button size="small" variant="contained" color="primary"
                                    style={{marginTop: '-70px', float: 'right',}}
                                    onClick={() => this.setState({popupDialogue2:true,id:val.id, username:val.username,
                                        password:val.password})}
                            >
                                Edit
                            </Button>
                            <Button size="small" variant="contained" color="Secondary"
                                    style={{marginTop: '-70px', float: 'right',}}
                                    onclick={this.deleteUser(val.id)}
                            >
                                Delete
                            </Button>
                            <Button size="small" style={{marginTop: '-70px', float: 'right',}}>
                            </Button>
                        </DialogActions>
                    </ListItem>
                    <Divider/>
                </List>
            )
        });

        return (
            <Container>
                <Dialog open={this.state.popupDialogue} aria-labelledby="simple-dialog-title">
                    <DialogTitle id="simple-dialog-title">Add user</DialogTitle>
                    <DialogContent style={{width: '500px'}}>
                        <form onSubmit={this.addUser}>
                            <TextField
                                required
                                name='username'
                                id="username"
                                label="UserName"
                                type="text"
                                value={this.state.username}
                            />
                            <br/>
                            <Button variant="contained" color="primary" type="submit"
                            >
                                Add User
                            </Button>

                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" onClick={() => this.setState({popupDialogue: false})}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>

                <SearchBox searchHandler={this.searchHandler}/>

                <Paper elevation={1} component="form">
                    <List style={{
                        marginBottom: '10px',
                        overflow: 'auto',
                        maxHeight: 700,

                    }}>
                        {userCatalog}
                    </List>
                </Paper>

                <Button variant="contained" color="primary" onClick={() => this.setState({popupDialogue: true})}>
                    Add User
                </Button>
            </Container>

        );
    }
}
