import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";

export class EditUserDialogue extends Component {
    constructor(props) {
        super(props);
    }

    editUser(event) {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/'+ event.target.id.value,
            {
                method: 'POST',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: event.target.id.value,
                    username: event.target.username.value, password: event.target.password.value
                })
            })

            .then(res=> res.json())
            .then((result) =>{alert('Success!');}
            )
    }

    render() {
        return (
            <Dialog open={this.props.open} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Edit User</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <form onSubmit={this.editUser}>
                        <TextField
                            name='id'
                            disabled
                            type="hidden"
                            defaultValue={this.props.id}
                        />
                        <br/>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            name='username'
                            label="username"
                            type="text"
                            defaultValue={this.props.username}
                        />
                        <br/>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            name='password'
                            label="Password"
                            type="text"
                        />
                        <br/>
                        <Button variant="contained" color="primary" type="submit" onClick={this.props.Close}>
                            update
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.Close}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }

}
