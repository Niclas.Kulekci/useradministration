import React, {Component} from "react";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";


function SearchBox(props) {

    return (

        <Box style={{marginBottom: '10px', display: 'flow-root'}} >
            <span className="colName">Users</span>
            <Paper component="form" style={{float: 'right', width: '500px' }}>
                <IconButton type="submit" aria-label="search">
                    <SearchIcon/>
                </IconButton>
                <InputBase
                    placeholder="Search for user"
                    inputProps={{'aria-label': 'User name'}}
                    onChange={props.searchHandler}
                    type="text"
                />
            </Paper>
        </Box>
    )
}
export default SearchBox;
