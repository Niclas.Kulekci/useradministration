import React, {Component} from "react";
import {EditUserDialogue} from "./Dialogue_EDIT";
import Container from "@material-ui/core/Container";
import SearchBox from "./Search";
import Paper from "@material-ui/core/Paper";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import Grid from "@material-ui/core/Grid";


function searchingFor(searchUser) {
    return function (val) {
        return val.username.toLowerCase().includes(searchUser.toLowerCase());
    }
}

class editPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            userPopDialogue: false,
            searchUser: '',
        }
    }

    deleteUser(id) {
        fetch('https://usersjava20.sensera.se/users/' + id, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(result => {
            alert('Success!');
        })
    }

    searchHandler = (event, searchUser) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/find?username=' + searchUser)
            .then(response => response.json())
        this.setState({searchUser: event.target.value})
    }

    refreshList = () => {
        fetch('https://usersjava20.sensera.se/users')
            .then(response => response.json())
            .then(data => {
                this.setState({users: data});
            })

    }

    componentDidMount = () => {
        this.refreshList();
    }

    render() {
        const {users} = this.state
        let DialogClose = () => this.setState({userPopDialogue: false});

        return (
            <Container>
                <EditUserDialogue
                    open={this.state.userPopDialogue}
                    username={this.state.username}
                    id={this.state.id}
                    password={this.state.password}
                    Close={DialogClose}/>

                <SearchBox searchHandler={this.searchHandler}/>

                <Paper component="nav" aria-label="main mailbox folders" style={{
                    marginBottom: '10px',
                    overflow: 'auto',
                    maxHeight: 700,
                }}>
                    <Grid container spacing={1}>
                        <Grid>
                            <List>
                                {users.filter(searchingFor(this.state.searchUser)).map((val) =>
                                    <ListItem key={val.id} style={{height: '70px'}}>
                                        <ListItemText primary={val.username}/>
                                    </ListItem>
                                )}
                            </List>
                        </Grid>

                        <Grid>
                            <List>
                                {users.filter(searchingFor(this.state.searchUser)).map((val) =>
                                    <ListItem key={val.id} style={{marginLeft: '100px', height: '70px'}}>

                                        <DialogActions style={{marginTop: '40px'}}>
                                            <Button size="small" variant="contained" color="primary"
                                                    style={{marginTop: '-70px', float: 'right',}}
                                                    onClick={() => this.setState({
                                                        userPopDialogue: true, id: val.id,
                                                        username: val.username, password: val.password
                                                    })}
                                            >
                                                Edit
                                            </Button>
                                        </DialogActions>

                                        <DialogActions style={{marginTop: '40px'}}>
                                            <Button size="small" variant="contained" color="secondary"
                                                    style={{marginTop: '-70px', float: 'right',}}
                                                    onClick={() => this.deleteUser(val.id)}
                                            >
                                                Delete
                                            </Button>
                                        </DialogActions>

                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                    </Grid>
                </Paper>
            </Container>
        );
    }
}

export default editPage;

