import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";

export class AddUserDialogue extends Component {


    addUser = (event) => {
        event.preventDefault();

        fetch('https://usersjava20.sensera.se/users',
            {
                method: 'POST',
                headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: event.target.username.value,password: 'BeatThis'
                })
            })

            .then(res=> res.json())
            .then((result) =>{alert('Submitted');},
                (error)=>{alert('failed')}
            )
    }

    render() {

        return (
            <Dialog
                open={this.props.open}
                aria-labelledby="simple-dialog-title"
            >
                <DialogTitle id="simple-dialog-title">Add user</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <form onSubmit={this.addUser}>
                        <TextField
                            required
                            autoFocus
                            name='username'
                            id="username"
                            label="user"
                            type="text"
                        />
                        <br/>
                        <Button variant="contained" color="primary" type="submit"
                                onClick={this.props.onClick}>
                            Add User
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.onClick}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>

        )
    }

}
