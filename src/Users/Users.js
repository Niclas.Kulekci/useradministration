import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import SearchBox from "./Search";
import {AddUserDialogue} from "./Dialogue_ADD";
import {Link} from "react-router-dom";


function searchingFor(searchUser) {
    return function (val) {
        return val.username.toLowerCase().includes(searchUser.toLowerCase());
    }
}

export class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            userPopDialogue: false,
            searchUser: '',
        }
    }

    searchHandler = (event, searchUser) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/find?username=' + searchUser)
            .then(response => response.json())
            .then(date => {
                this.setState({username: date});
            })
        this.setState({searchUser: event.target.value})
    }

    refreshList = () => {

        fetch('https://usersjava20.sensera.se/users')
            .then(response => response.json())
            .then(date => {
                this.setState({users: date});
            })
    }

    componentDidMount = () => {
        this.refreshList();
    }

    render() {
        const {users} = this.state
        let addUserDialogueClose = () => this.setState({userPopDialogue: false});

        return (
            <Container>
                <SearchBox searchHandler={this.searchHandler}/>
                <Paper elevation={1} component="form">
                    <List component="nav" aria-label="main mailbox folders" style={{
                        marginBottom: '10px',
                        overflow: 'auto',
                        maxHeight: 700,
                    }}>
                        {users.filter(searchingFor(this.state.searchUser)).map((val) =>
                            <ListItem key={val.id} style={{}}>
                                <ListItemText primary={val.username}/>
                            </ListItem>
                        )}
                    </List>

                </Paper>
                <Button variant="contained" color="primary" type="submit"
                        onClick={() => this.setState({userPopDialogue: true})}>
                    Add User
                </Button>
                <Button variant="contained" color="secondary" component={Link} to="/SearchEdit" style={{marginLeft:'30px'}}>
                    Edit
                </Button>

                <AddUserDialogue
                    open={this.state.userPopDialogue}
                    onClick={addUserDialogueClose}
                />
            </Container>

        );
    }
}
