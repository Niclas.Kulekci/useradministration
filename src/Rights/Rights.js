import React, {Component} from "react";
import '../App.css';
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import {AddRights} from './Dialogue_ADD_Rights'
import Grid from '@material-ui/core/Grid';
//import SearchBox3 from "./Search";


/*
function searchingFor(searchRights) {

    return function (val) {
        return val.rights.toLowerCase().includes(searchRights.toLowerCase());
    }
}
//server does not have search function for rights
*/

class Rights extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rights: [],
            rightsPopDialogue: false,

        }
    }

    /*
    searchHandlerRights = (event, searchRights) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/rights/find?rights=' + searchRights)
            .then(response => response.json())
            .then(date => {
                this.setState({rights: date});
            })
        this.setState({searchRights: event.target.value})
    }
       <SearchBox3 searchHandlerRights={this.searchHandlerRights}/> <----- this goes under "<container>"
    */

    componentDidMount = () => {
        this.refreshList();
    }

    refreshList = () => {
        fetch('https://usersjava20.sensera.se/rights')
            .then(response => response.json())
            .then(date => {
                this.setState({rights: date});
            })

    }

    render() {
        const {rights} = this.state;
        let DialogClose = () => this.setState({rightsPopDialogue: false});

        return (
            <Container>

                <Paper component="nav" aria-label="main mailbox folders" style={{
                    marginBottom: '10px',
                    overflow: 'auto',
                    maxHeight: 700,
                }}>

                    <Grid container spacing={1} >

                        <Grid item xs={8} md={2}>
                            <List >
                                <span style={{marginLeft: '10px',borderBottom:'1px solid red'}} >DOMAINS</span>
                                {rights/*filter(searchingFor(this.state.searchRights))*/.map((val) =>
                                    <ListItem key={val.id} style={{height: '70px'}}>
                                        <ListItemText
                                            primary={val.domain}
                                        />
                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                        <Grid item xs={8} md={2}>
                            <List >
                                <span style={{marginLeft: '10px',borderBottom:'1px solid red'}} >RIGHTS</span>
                                {rights.map((val) =>
                                    <ListItem key={val.id} style={{height: '70px'}}>
                                        <ListItemText primary={val.key} />
                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                        <Grid>
                            <List >
                                <span style={{marginLeft: '10px',borderBottom:'1px solid red'}} >ID</span>
                                {rights.map((val) =>
                                    <ListItem key={val.id} style={{height: '70px'}}>
                                        <ListItemText
                                            primary={val.id}/>
                                    </ListItem>,
                                )}
                            </List>
                        </Grid>
                    </Grid>
                </Paper>
                <Button variant="contained" color="primary" onClick={() => this.setState({rightsPopDialogue: true})}>
                    Add Rights
                </Button>


                <AddRights
                    open={this.state.rightsPopDialogue}
                    key={this.state.key}
                    domain={this.state.domain}
                    Close={DialogClose}/>
            </Container>

        );
    }

}
export default Rights;
