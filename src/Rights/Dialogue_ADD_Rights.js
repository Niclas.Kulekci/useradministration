import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";


export class AddRights extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: [],
        }
    }

    addRights = (event) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/rights',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    key: event.target.key.value,
                    domain: event.target.domain.value,
                })
            })
            .then(response => {
                return response.json()
            });
    }

    refreshList2 = () => {
        fetch('https://usersjava20.sensera.se/domains')
            .then(response => response.json())
            .then(date => {
                this.setState({domains: date});
            })
    }

    componentDidMount = () => {
        this.refreshList2();
    }

    handleChange = (event) => {
        this.setState({domain: event.target.value});
    }
    handleChange2 = (event) => {
        this.setState({key: event.target.value});
    }


    render() {
        return (
            <Dialog open={this.props.open} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add Rights</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <form onSubmit={this.addRights}>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            name='domain'
                            label="domain"
                            type="text"
                            value={this.props.domain}
                            onChange={this.handleChange}/>
                        Domains NAME goes here<br/>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            name='key'
                            label="rights"
                            type="text"
                            value={this.props.key}
                            onChange={this.handleChange2}/>
                            Domains RIGHTS goes here<br/>
                        <Button variant="contained" color="primary" type="submit" style={{marginTop: '10px'}}
                                onClick={this.props.Close}
                        >
                            Update
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.Close}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}