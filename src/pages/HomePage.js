import React from "react";
import '../App.css';
import Container from "@material-ui/core/Container";



export default function HomePage() {

    return (
        <Container className="pageTitle">
           <h1> Welcome to the ultimate school assignment! </h1>
        </Container>
    );
}


